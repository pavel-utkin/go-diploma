module go-diploma

go 1.19

require (
	github.com/caarlos0/env/v6 v6.10.1
	github.com/go-chi/chi/v5 v5.0.8
	github.com/golang-migrate/migrate/v4 v4.15.2
	github.com/jackc/pgconn v1.14.0
	github.com/jackc/pgerrcode v0.0.0-20220416144525-469b46aa5efa
	github.com/jackc/pgx/v4 v4.18.1
	golang.org/x/crypto v0.9.0
)

require (
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/jackc/chunkreader/v2 v2.0.1 // indirect
	github.com/jackc/pgio v1.0.0 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgproto3/v2 v2.3.2 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	github.com/jackc/pgtype v1.14.0 // indirect
	github.com/lib/pq v1.10.2 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	golang.org/x/text v0.9.0 // indirect
)
